/*
 * Copyright (C) 2022 MURENA SAS
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.moez.QKSMS.util

import android.os.Handler
import android.os.Looper
import java.io.File

object FileUtils {

    fun moveDir(sourceDir: File, destinationDir: File) {
        if (!sourceDir.exists() || !sourceDir.isDirectory) {
            return
        }

        // Perform the copy operation asynchronously using
        // a Handler to prevent blocking the main thread
        Handler(Looper.getMainLooper()).post {
            if (destinationDir.exists()) {
                val copySuccess = sourceDir.copyRecursively(destinationDir, true)
                if (copySuccess) {
                    sourceDir.deleteRecursively()
                }
            } else {
                sourceDir.renameTo(destinationDir)
            }
        }
    }

}
