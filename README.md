# Message

Message is an open source replacement to the stock messaging app on Android.
Message is forked from [QKSMS](https://github.com/moezbhatti/qksms)

## Authors

[Authors](https://gitlab.e.foundation/e/apps/Message/-/blob/master/AUTHORS)

## Release Notes

Check out the [Release Notes](https://gitlab.e.foundation/e/apps/Message/-/releases) to find out what changed
in each version of Message.

## Privacy Policy

[Privacy Policy](https://e.foundation/legal-notice-privacy)
[Terms of service](https://e.foundation/legal-notice-privacy)

## License

Message is released under the [The GNU General Public License v3.0 (GPLv3)](https://gitlab.e.foundation/e/apps/Message/-/blob/master/LICENSE)

